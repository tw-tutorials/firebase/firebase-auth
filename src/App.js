import React, { Component } from 'react';
import './App.css';
import firebase from 'firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';

firebase.initializeApp({
	apiKey: 'AIzaSyCmBMEq-uGN0xgk7y3pyN8b2sW8lYk5klY',
	authDomain: 'twtest-firebase-auth.firebaseapp.com'
});

class App extends Component {
	state = {
		isSignedIn: false,
		uiConfig: {
			signInFlow: 'popup',
			signInOptions: [
				firebase.auth.GoogleAuthProvider.PROVIDER_ID,
				firebase.auth.FacebookAuthProvider.PROVIDER_ID,
				firebase.auth.GithubAuthProvider.PROVIDER_ID,
				firebase.auth.EmailAuthProvider.PROVIDER_ID
			],
			callbacks: {
				signInSuccess: () => false
			}
		}
	};

	componentDidMount() {
		firebase.auth().onAuthStateChanged(async (user) => {
			this.setState({ isSignedIn: !!user });
			if (!!user) {
				console.log(await user.getIdToken());
			}
		});
	}

	render() {
		return (
			<div className="App">
				{this.state.isSignedIn ? (
					<span>
						<div>Signed in!</div>
						<button onClick={() => firebase.auth().signOut()}>Sign out!</button>
						<h1>Welcome {firebase.auth().currentUser.displayName}</h1>
						<img src={firebase.auth().currentUser.photoURL} alt="profile picture" />
					</span>
				) : (
					<StyledFirebaseAuth
						uiConfig={this.state.uiConfig}
						firebaseAuth={firebase.auth()}
					></StyledFirebaseAuth>
				)}
			</div>
		);
	}
}

export default App;
